﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComponent : MonoBehaviour {
    
    enum Direction {
        NONE,
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    [SerializeField] float playerSpeed = 5f;
    [SerializeField] float rotationSpeed = 5f;

    Rigidbody rigidBody;
    Animator animator;
    float xVelocity, zVelocity;
    Vector3 moveInput;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        HandleInput();
    }

    private void FixedUpdate() {
        Move();
        Rotate();
    }

    private void Move() {
        rigidBody.velocity = Vector3.Normalize(moveInput) * playerSpeed;
    }

    private void Rotate() {
        Vector3 rotation = new Vector3();
        if (moveInput.z > 0f) {
            rotation.y = 0f;
            if (moveInput.x > 0f) {
                rotation.y = 45f;
            } else if (moveInput.x < 0f) {
                rotation.y = -45f;
            }
        } else if (moveInput.z < 0f) {
            rotation.y = 180f;
            if (moveInput.x > 0f) {
                rotation.y = 135f;
            } else if (moveInput.x < 0f) {
                rotation.y = -135f;
            }
        } else {
            if (moveInput.x > 0f) {
                rotation.y = 90f;
            } else if (moveInput.x < 0f) {
                rotation.y = -90f;
            }
        }

        if (moveInput.magnitude > 0f) {
            rigidBody.rotation = Quaternion.Slerp(rigidBody.rotation, Quaternion.Euler(rotation), Time.deltaTime * rotationSpeed);
        }
    }

    private void HandleInput() {
        moveInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        animator.SetFloat("Speed", moveInput.magnitude);
    }
}
